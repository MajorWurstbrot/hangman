#include "game.h"
#include "ui_game.h"
#include <QTcpSocket>
#include <QJsonArray>
#include <QJsonValue>
#include "hangmanjsonobject.h"
#include <QPixmap>
#include <QDebug>

game::game(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::game)
{
    ui->setupUi(this);
    loggedIn = false;
    ui->btnSendWord->setEnabled(false);
    ui->btnSendLetter->setEnabled(false);
    QColor red(Qt::red);
    redColor.setColor(red);
    QColor green(Qt::green);
    greenColor.setColor(green);
    img = "images\\00.png";
    ui->lblImg->setAlignment(Qt::AlignCenter);
    if(imgPixmap.load(img))
    {
        imgPixmap = imgPixmap.scaled(ui->lblImg->size(), Qt::KeepAspectRatio);
        ui->lblImg->setPixmap(imgPixmap);
    }
}

game::~game()
{
    client->disconnectFromHost();
    client->close();
    delete ui;
}

void game::setLoggedIn(bool loggedIn)
{
    this->loggedIn = loggedIn;
}

void game::setSocket(QTcpSocket *client)
{
    this->client = client;

    jObjToSend = HangManJsonObject::listPlayers();
    QString str = HangManJsonObject::toByteArray(jObjToSend);
    qDebug() << "list players: " + str;
    client->write(str.toUtf8());

    jObjToSend = HangManJsonObject::state();
    str = HangManJsonObject::toByteArray(jObjToSend);
    qDebug() << "state: " + str;
    client->write(str.toUtf8());

    //jObjToSend = HangManJsonObject::ping();
    //str = HangManJsonObject::toByteArray(jObjToSend);
    //    while(true)
    //        client->write(str.toUtf8());
}

void game::handleResponse(QJsonObject jObj)
{
    //qDebug() << "handle response";
    QString responseType = jObj["type"].toString();
    if (responseType == "RoundStartedEvent"){
        qDebug() << "round start event";
        this->myWrongGuesses = 0;
        ui->lblWrongGuesses->setText(QString::number(this->myWrongGuesses));
        wordLength = 0;
        ui->btnSendWord->setEnabled(true);
        ui->btnSendLetter->setEnabled(true);

        wordLength = jObj["wordLength"].toInt();
        stringToShow = generateStringWithLength(wordLength);
        ui->lblWord->setText(stringToShow);
    }
    else if(loggedIn){
        if (responseType == "ListPlayersResponse") {
            qDebug() << "list players response";
            QJsonArray playerList = jObj["players"].toArray();
            ui->lwPlayers->clear();
            for (QJsonValue v : playerList)
            {
                ui->lwPlayers->addItem(v.toString());
            }
        }
        else if (responseType == "PlayerJoinedEvent"){
            qDebug() << "player joined event";
            QString playerName = jObj["playerName"].toString();
            ui->lwPlayers->addItem(playerName);
        }
        else if (responseType == "PlayerLeftEvent"){
            qDebug() << "player left event";
            ui->lwPlayers->clear();
            jObjToSend = HangManJsonObject::listPlayers();
            QString str = HangManJsonObject::toByteArray(jObjToSend);
            client->write(str.toUtf8());

            //            QString playerName = jObj["playerName"].toString();
            //            QList<QListWidgetItem*> items = ui->lwPlayers->findItems(playerName,Qt::MatchExactly);
            //            for (QListWidgetItem* i : items)
            //                ui->lwPlayers->removeItemWidget(i);
        }
        else if (responseType == "RoundStartedEvent") {

        }
        else if (responseType == "CorrectGuessEvent") {
            qDebug() << "correct guess event";
            QJsonObject jObject = HangManJsonObject::state();
            QString str = HangManJsonObject::toByteArray(jObject);
            client->write(str.toUtf8());
        }
        else if (responseType == "WrongGuessEvent") {
            qDebug() << "wrong guess event";
            QJsonObject jObject = HangManJsonObject::state();
            QString str = HangManJsonObject::toByteArray(jObject);
            client->write(str.toUtf8());
        }
        else if (responseType == "StateResponse") {
            qDebug() << "state response";
            QJsonArray players = jObj["players"].toArray();
            ui->lwPlayers->clear();
            for (QJsonValue v : players)
            {
                ui->lwPlayers->addItem(v.toString());
            }

            wordLength = jObj["wordLength"].toInt();
            QJsonArray chars = jObj["chars"].toArray();

            stringToShow = generateStringWithChars(chars);
            ui->lblWord->setText(stringToShow);

            ui->lwGuesses->clear();

            QJsonArray invalidChars = jObj["invalidChars"].toArray();
            for (QJsonValue v : invalidChars)
                ui->lwGuesses->addItem(v.toString());
            QJsonArray invalidWords = jObj["invalidWords"].toArray();
            for (QJsonValue v : invalidWords)
                ui->lwGuesses->addItem(v.toString());

            myWrongGuesses = jObj["wrongGuesses"].toInt();
            ui->lblWrongGuesses->setText(QString::number(myWrongGuesses));

            img = "images\\0" + QString::number(myWrongGuesses) + ".png";
            ui->lblImg->setAlignment(Qt::AlignCenter);
            if(imgPixmap.load(img))
            {
                imgPixmap = imgPixmap.scaled(ui->lblImg->size(), Qt::KeepAspectRatio);
                ui->lblImg->setPixmap(imgPixmap);
            }

        }
        else if (responseType == "CheckGuessResponse") {
            qDebug() << "check guess response";
//            jObjToSend = HangManJsonObject::state();
//            QString str = HangManJsonObject::toByteArray(jObjToSend);
//            client->write(str.toUtf8());
        }
        else if (responseType == "RoundEndedEvent") {
            qDebug() << "round ended event";
            ui->btnSendWord->setEnabled(false);
            ui->btnSendLetter->setEnabled(false);

            ui->lblWord->setText(jObj["correctWord"].toString());
            ui->lblError->setText("Winner: " + jObj["winner"].toString());
        }
        else if (responseType == "ErrorResponse") {
            qDebug() << "error response";
            QString errorMsg = jObj["message"].toString();
            ui->lblError->setText(errorMsg);
        }
        else {
            //genau
            qDebug() << "reponse type: " + responseType;
        }

    }
}

void game::on_edtLetter_textChanged(const QString &arg1)
{
    QString text = arg1;
    text = text.toLower();
    int letter = text[0].toLatin1();
    if(!(letter >= 97 && letter <= 122)){
        ui->edtLetter->setText("");
    }
}

void game::on_btnSendLetter_clicked()
{
    if(ui->edtLetter->text() != ""){
        jObjToSend = HangManJsonObject::sendGuess(false, ui->edtLetter->text());
        ui->edtLetter->clear();
        QString str = HangManJsonObject::toByteArray(jObjToSend);
        client->write(str.toUtf8());}
}

void game::on_btnSendWord_clicked()
{
    if(ui->edtGuessWord->text() != ""){
        jObjToSend = HangManJsonObject::sendGuess(true, ui->edtGuessWord->text());
        ui->edtGuessWord->clear();
        QString str = HangManJsonObject::toByteArray(jObjToSend);
        client->write(str.toUtf8());}
}

QString game::generateStringWithLength(int wordLength)
{
    QString returnValue = "";
    for (int i = 0; i < wordLength; i++)
        returnValue += "_ ";
    return returnValue;
}

QString game::generateStringWithChars(QJsonArray chars)
{
    QString returnValue = "";
    for(int i = 0; i < chars.size(); i++)
    {
        if(chars.at(i).isString())
            returnValue += chars.at(i).toString() + " ";
        else
            returnValue += "_ ";

    }
    return returnValue;

}
