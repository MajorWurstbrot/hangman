/*
 * Datei: hangmanjsonobject.h
 * Klasse: HangManJsonObject
 * Funktion: JSON-Objekte fuer Hang Man
 * Autor: Peter Gruening
 * Datum: 2020-05-13
 * */

#ifndef HANGMANJSONOBJECT_H
#define HANGMANJSONOBJECT_H


#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>

class HangManJsonObject
{
public:
    HangManJsonObject();
    static QJsonObject ping();
    static QJsonObject login(QString user, QString pwd);
    static QByteArray toByteArray(const QJsonObject &jObj);
    static QJsonObject toJson(const QByteArray &array);
    static QJsonObject listPlayers();
    static QJsonObject state();
    static QJsonObject sendGuess(bool word, QString guess);
};

#endif // HANGMANJSONOBJECT_H
