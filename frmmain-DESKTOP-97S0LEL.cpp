#include "frmmain.h"
#include "ui_frmmain.h"
#include <QTcpSocket>
#include <QString>
#include <QDebug>

FrmMain::FrmMain(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::FrmMain)
{
    ui->setupUi(this);
    client = new QTcpSocket();
    connect(client, SIGNAL(readyRead()),this,SLOT(on_client_lesen()));
    connect(client, SIGNAL(connected()), this, SLOT(on_connected()));
    //connect(client, SIGNAL(error()), this, SLOT(on_connect_error()));
    ui->txtUsername->setPlaceholderText("Username");
    ui->txtPassword->setPlaceholderText("Password");
    ui->lblConnected->setVisible(false);
    ui->txtPassword->setVisible(false);
    ui->txtUsername->setVisible(false);
    ui->btnLogin->setVisible(false);
    ui->btnCancel->setVisible(false);
    loggedIn = false;
    gameFrame = new game();
    gameFrame->setVisible(false);
}

FrmMain::~FrmMain()
{
    client->disconnectFromHost();
    client->close();
    delete ui;
}

void FrmMain::on_client_lesen()
{
    QString response = client->readAll();
    qDebug() << "response: " + response;

    QStringList responses = response.split("\n");
    for(QString str : responses){
    if(str == "")
        return;
    jObj = HangManJsonObject::toJson(str.toUtf8());
    QString responseType = jObj["type"].toString();
    if(!loggedIn && responseType != "RoundStartedEvent"){
        if (responseType == "LoginResponse") {
            if (jObj["ok"].toBool()){
                ui->lblConnected->setText("Erfolgreich eingeloggt");
                loggedIn = true;
                this->setVisible(false);
                gameFrame->setVisible(true);
                gameFrame->setLoggedIn(loggedIn);
                gameFrame->setSocket(client);
            }
            else {
                ui->lblConnected->setText("Eingaben fehlerhaft");
            }
        }
    }
    else
        gameFrame->handleResponse(jObj);
    }
}


void FrmMain::on_btnConnect_clicked()
{
    ui->btnConnect->setEnabled(false);
    ui->btnConnect->setVisible(false);
    ui->lblConnected->setVisible(true);
    ui->lblConnected->setText("Verbindung zum Server wird hergestellt");
    //TODO Verbindung zum Server aufbauen
    //QString serverAdress = "10.3.141.185";
    //QString serverAdress = "127.0.0.1";
    QString serverAdress = "jonnyB.name";
    int serverPort = 12345;
    client->connectToHost(serverAdress, serverPort);
}

void FrmMain::on_btnCancel_clicked()
{
    ui->btnConnect->setVisible(true);
    ui->btnConnect->setEnabled(true);
    ui->lblConnected->setVisible(false);
    ui->txtPassword->setVisible(false);
    ui->txtUsername->setVisible(false);
    ui->btnLogin->setVisible(false);
    ui->btnCancel->setVisible(false);
    ui->txtPassword->setText("");
    ui->txtUsername->setText("");
    client->close();
}

void FrmMain::on_btnLogin_clicked()
{
    QString username;
    QString password;
    username = ui->txtUsername->text();
    password = ui->txtPassword->text();
    if (username != "" && password != "")
    {
        jObj = HangManJsonObject::login(username, password);
        QString str = HangManJsonObject::toByteArray(jObj);
        client->write(str.toUtf8());
    }
    else
    {
        ui->lblConnected->setText("Fehlerhafte Eingabe");
    }
}

void FrmMain::on_connected()
{
    connected = true;
    ui->btnConnect->setVisible(false);
    ui->txtPassword->setVisible(true);
    ui->txtUsername->setVisible(true);
    ui->btnLogin->setVisible(true);
    ui->btnCancel->setVisible(true);
    ui->lblConnected->setText("Verbindung zum Server wurde hergestellt");
}

void FrmMain::on_connect_error()
{
    ui->btnConnect->setEnabled(true);
    ui->btnConnect->setVisible(true);
    connected = false;
    ui->lblConnected->setText("Verbindung zum Server konnte nicht hergestellt werden");
    this->repaint();
    ui->lblConnected->repaint();
}
