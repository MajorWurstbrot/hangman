#ifndef GAME_H
#define GAME_H

#include <QWidget>
#include <QTcpSocket>
#include <QJsonObject>
#include <QString>
#include <QVector>
#include <QPixmap>

namespace Ui {
class game;
}

class game : public QWidget
{
    Q_OBJECT

public:
    explicit game(QWidget *parent = nullptr);
    ~game();
    void setLoggedIn(bool loggedIn);
    void setSocket(QTcpSocket* client);
    void handleResponse(QJsonObject jObj);

private slots:
    void on_edtLetter_textChanged(const QString &arg1);

    void on_btnSendLetter_clicked();

    void on_btnSendWord_clicked();

private:
    Ui::game *ui;
    bool loggedIn;
    QTcpSocket* client;
    QBrush redColor;
    QBrush greenColor;
    QJsonObject jObjToSend;
    QString img;
    QPixmap imgPixmap;

    //game Variables
    QString stringToShow;
    int wordLength;
    int myWrongGuesses;

    QString generateStringWithLength(int wordLength);
    QString generateStringWithChars(QJsonArray chars);

};

#endif // GAME_H
