#ifndef FRMMAIN_H
#define FRMMAIN_H
#include <QTcpSocket>
#include "hangmanjsonobject.h"
#include <QJsonObject>
#include "game.h"

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class FrmMain; }
QT_END_NAMESPACE

class FrmMain : public QMainWindow
{
    Q_OBJECT

public:
    FrmMain(QWidget *parent = nullptr);
    ~FrmMain();

private:
    Ui::FrmMain *ui;
    QTcpSocket* client;
    bool connected = false;
    QJsonObject jObj;
    bool loggedIn;
    game* gameFrame;

private slots:
    void on_client_lesen();
    void on_btnConnect_clicked();
    void on_btnCancel_clicked();
    void on_btnLogin_clicked();
    void on_connected();
    void on_connect_error();
};
#endif // FRMMAIN_H
