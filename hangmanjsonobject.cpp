/*
 * Datei: hangmanjsonobject.cpp
 * Klasse: HangManJsonObject - Methoden
 * Funktion: JSON-Objekte fuer Hang Man
 * Autor: Peter Gruening
 * Datum: 2020-05-13
 * */


#include "hangmanjsonobject.h"

HangManJsonObject::HangManJsonObject()
{
}

QJsonObject HangManJsonObject::ping()
{
    QJsonObject jObj;

    //"{ \"command\" : \"ping\" }";

    jObj.insert("command","ping");
    return jObj;
}

QJsonObject HangManJsonObject::login(QString user, QString pwd)
{
    QJsonObject jObj;
    QJsonObject jTmp;

    jObj.insert("command","login");

    //payload
    jTmp.insert("username",user);
    jTmp.insert("password",pwd);
    jObj.insert("payload",jTmp);

    return jObj;
}

QByteArray HangManJsonObject::toByteArray(const QJsonObject &jObj)
//Verwendung: zum Versenden per Socket
{
    QByteArray array;
    int pos;
    QJsonDocument jDoc(jObj);

    array = jDoc.toJson();

    //new lines entfernen
    pos = array.indexOf('\n');
    while (pos>=0)
    {
        array.remove(pos,1);
        pos = array.indexOf('\n');
    }

    //    //spaces entfernen
    //    pos = array.indexOf(' ');
    //    while (pos>=0)
    //    {
    //        array.remove(pos,1);
    //        pos = array.indexOf(' ');
    //    }
    array.append('\n');
    return array;
}

QJsonObject HangManJsonObject::toJson(const QByteArray &array)
//Verwendung: Konvertierung in JSON aus Empfang per Socket
{
    QJsonDocument jDoc;
    QJsonParseError *error =  nullptr;

    jDoc = QJsonDocument::fromJson(array,error);

    if (error)
    {
        QString x = error->errorString().toLatin1();
        qDebug() << x;
    }

    return jDoc.object();
}

QJsonObject HangManJsonObject::listPlayers()
{
    QJsonObject jObj;

    jObj.insert("command","ListPlayers");

    return jObj;
}

QJsonObject HangManJsonObject::state()
{
    QJsonObject jObj;
    jObj.insert("command", "State");
    return jObj;
}

QJsonObject HangManJsonObject::sendGuess(bool word, QString guess)
{
    QJsonObject jObj;
    jObj.insert("command", "CheckGuess");

    QJsonObject jTmp;
    if (word)
        jTmp.insert("guessType", "WORD");
    else
        jTmp.insert("guessType","CHAR");
    jTmp.insert("content", guess);
    jObj.insert("payload",jTmp);

    return jObj;
}

